package ru.volnenko.se.api.service;

import ru.volnenko.se.entity.Task;

import java.util.List;

/**
 * @author Denis Volnenko
 */
public interface ITaskService {

    List<Task> findAll();

    Task createTask(String name);

    void deleteAll();

}
