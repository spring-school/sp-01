package ru.volnenko.se.api.service;

import ru.volnenko.se.entity.Project;

import java.util.Collection;
import java.util.List;

/**
 * @author Denis Volnenko
 */
public interface IProjectService {

    List<Project> findAll();

    Project createProject(String name);

    void deleteAll();

}
