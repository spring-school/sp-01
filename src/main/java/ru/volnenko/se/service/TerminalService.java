package ru.volnenko.se.service;

import org.springframework.stereotype.Service;
import ru.volnenko.se.api.service.ITerminalService;

import java.util.Scanner;

/**
 * @author Denis Volnenko
 */

@Service
public final class TerminalService implements ITerminalService {

    private final Scanner scanner = new Scanner(System.in);

    public String nextLine() {
        return scanner.nextLine();
    }

    public Integer nextInteger() {
        final String value = nextLine();
        if (value == null || value.isEmpty()) return null;
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return null;
        }
    }

}
