package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.repository.ProjectRepository;
import ru.volnenko.se.repository.TaskRepository;

import java.util.List;

/**
 * @author Denis Volnenko
 */

@Service
@Transactional
public class TaskService implements ru.volnenko.se.api.service.ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task createTask(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        return taskRepository.save(task);
    }

    @Override
    public void deleteAll() {
        taskRepository.deleteAll();
    }


}
