package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.repository.ProjectRepository;

import java.util.List;

/**
 * @author Denis Volnenko
 */

@Service
@Transactional
public class ProjectService implements ru.volnenko.se.api.service.IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Project createProject(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        return projectRepository.save(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void deleteAll() {
        projectRepository.deleteAll();
    }


}
