package ru.volnenko.se.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IDomainService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.dto.Domain;

import java.io.IOException;
import java.io.InputStream;


@Component
public final class FromJsonCommand extends AbstractCommand {

    @Autowired
    private IDomainService domainService;

    @Override
    public String command() {
        return "from-json";
    }

    @Override
    public String description() {
        return "Load data from JSON";
    }

    @Override
    public void execute() throws IOException {

        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("domain.json");
        final ObjectMapper mapper = new ObjectMapper();
        final Domain domain = mapper.readValue(inputStream, Domain.class);

        domainService.setDomain(domain);
        System.out.println("[DATA LOADED]");
        System.out.println();
    }
}
