package ru.volnenko.se.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.api.service.ITerminalService;
import ru.volnenko.se.command.AbstractCommand;

/**
 * @author Denis Volnenko
 */

@Component
public final class TaskCreateCommand extends AbstractCommand {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private ITerminalService terminalService;

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = terminalService.nextLine();

        taskService.createTask(name);
        System.out.println("[OK]");

        System.out.println();
    }

}
